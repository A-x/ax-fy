var coffee = require('coffee');
var path = require('path');
var version = require('../package').version;

describe('ax-fy', function() {
  it('should show help info', function(done) {
    coffee.fork(path.join(__dirname, '../bin/ax-fy'))
      .expect('stdout', 'ax-fy ~ ' + version + '\nTranslate tools in command line\n  $ ax-fy word\n  $ ax-fy world peace\n  $ ax-fy chinglish\n')
      .expect('code', 0)
      .end(done);
  });

  it('should translate word', function(done) {
    coffee.fork(path.join(__dirname, '../bin/ax-fy'), ['word'])
      .expect('stdout', /word  \[ wɜːd \]  ~  ax-fy\.youdao\.com/gi)
      .expect('code', 0)
      .end(done);
  });
});

